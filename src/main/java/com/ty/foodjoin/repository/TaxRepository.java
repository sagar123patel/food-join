package com.ty.foodjoin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ty.foodjoin.dto.Tax;

public interface TaxRepository extends JpaRepository<Tax, Integer>{

	@Query("select t from Tax t where id=1")
	public List<Tax> getTax();
}
