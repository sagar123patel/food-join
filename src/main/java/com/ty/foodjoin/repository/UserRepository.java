package com.ty.foodjoin.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ty.foodjoin.dto.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("Select u from User u WHERE u.email =?1 AND u.password =?2")
	public User validateUser(String email,String password);
	
	
	@Query("SELECT u FROM User u WHERE u.name = ?1")
	public Optional<User> findByName(String name);
}
