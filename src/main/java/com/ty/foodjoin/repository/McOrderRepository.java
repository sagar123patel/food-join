package com.ty.foodjoin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.foodjoin.dto.McOrder;

public interface McOrderRepository extends JpaRepository<McOrder, Integer> {

}
