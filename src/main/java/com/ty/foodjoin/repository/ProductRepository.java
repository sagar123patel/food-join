package com.ty.foodjoin.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ty.foodjoin.dto.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{

	
	@Query("SELECT u FROM User u WHERE u.name = ?1")
	public Optional<Product> findByName(String name);
}
