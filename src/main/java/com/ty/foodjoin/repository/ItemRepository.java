package com.ty.foodjoin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ty.foodjoin.dto.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
