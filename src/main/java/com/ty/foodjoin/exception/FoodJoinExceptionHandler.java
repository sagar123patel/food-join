 package com.ty.foodjoin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ty.foodjoin.util.ResponseStructure;

@ControllerAdvice
public class FoodJoinExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(IDNotFoundException.class)
	public ResponseEntity<ResponseStructure<String>> handleUserNotFoundException(IDNotFoundException exception){
		ResponseStructure<String> structure =new ResponseStructure<String>();
		structure.setStatus(HttpStatus.NOT_FOUND.value());
		structure.setMessage(exception.getMessage());
		structure.setData("Exception : No ID found/exist");
		return new ResponseEntity<ResponseStructure<String>>(structure,HttpStatus.NOT_FOUND);
	}
}
