package com.ty.foodjoin.dao;

import java.util.List;

import com.ty.foodjoin.dto.Item;

public interface ItemDao {

	public Item saveItem(Item item);
	
	public Item getItemById(int id);
	
	public List<Item> getAllItem();
	
	public Item updateItem(int id,Item item);
	
	public boolean deleteItem(int id);
}
