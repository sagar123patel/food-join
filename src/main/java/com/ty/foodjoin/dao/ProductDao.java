package com.ty.foodjoin.dao;

import java.util.List;

import com.ty.foodjoin.dto.Product;

public interface ProductDao {

	public Product saveProduct(Product product);

	public Product getProductById(int id);

	public List<Product> getAllProducts();

	public Product updateProduct(int id, Product product);

	public boolean deleteProduct(int id);
	
	public Product getProductByName(String name);

}
