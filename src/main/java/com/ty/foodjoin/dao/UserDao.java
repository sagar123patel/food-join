package com.ty.foodjoin.dao;

import java.util.List;

import com.ty.foodjoin.dto.User;

public interface UserDao {
	
	public User saveUser(User user);
	
	public User getUserById(int id);
	
	public List<User> getAllUsers();
	
	public User updateUser(int id,User user);
	
	public boolean deleteUser(int id);
	
	public User validateUser(String email, String password);
	
	public User getUserByName(String name);
}
