package com.ty.foodjoin.dao;

import java.util.List;

import com.ty.foodjoin.dto.Tax;

public interface TaxDao {

	public Tax saveTax(Tax tax);
	
	public Tax getTaxById(int id);
	
	public List<Tax> getAllTaxs();
	
	public Tax updateTax(int id,Tax tax);
	
	public boolean deleteTax(int id);
	
}
