package com.ty.foodjoin.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoin.dao.McOrderDao;
import com.ty.foodjoin.dto.McOrder;
import com.ty.foodjoin.repository.McOrderRepository;

@Repository
public class McOrderDaoImpl implements McOrderDao {

	@Autowired
	private McOrderRepository mcOrderRepository;

	@Override
	public McOrder saveMcOrder(McOrder mcOrder) {
		McOrder mcOrderRep = (McOrder) mcOrderRepository.save(mcOrder);
		return mcOrderRep;
	}

	@Override
	public McOrder getMcOrderById(int id) {
		Optional<McOrder> optional = mcOrderRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public List<McOrder> getAllMcOrders() {
		return mcOrderRepository.findAll();
	}

	@Override
	public McOrder updateMcOrders(int id, McOrder mcOrder) {
		McOrder existingMcOrder = getMcOrderById(id);
		if (existingMcOrder != null) {
			existingMcOrder.setCust_name(mcOrder.getCust_name());
			existingMcOrder.setCust_phone(mcOrder.getCust_phone());
			existingMcOrder.setDateTime(mcOrder.getDateTime());
			existingMcOrder.setStatus(mcOrder.getStatus());
			existingMcOrder.setTotalCost(mcOrder.getTotalCost());
			return mcOrderRepository.save(existingMcOrder);
		}
		return null;
	}

	@Override
	public boolean deleteMcOrder(int id) {
		McOrder mcOrder = getMcOrderById(id);
		if (mcOrder != null) {
			mcOrderRepository.delete(mcOrder);
			return true;
		} else {
			return false;
		}
	}

}
