package com.ty.foodjoin.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoin.dao.ItemDao;
import com.ty.foodjoin.dto.Item;
import com.ty.foodjoin.repository.ItemRepository;

@Repository
public class ItemDaoImp implements ItemDao {

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Item saveItem(Item item) {
		Item itemRep = (Item) itemRepository.save(item);
		return itemRep;
	}

	@Override
	public Item getItemById(int id) {
		Optional<Item> optional = itemRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public List<Item> getAllItem() {
		return itemRepository.findAll();
	}

	@Override
	public Item updateItem(int id, Item item) {
		Item existingItem = getItemById(id);
		if(existingItem !=null) {
			existingItem.setName(item.getName());
			existingItem.setDescription(item.getDescription());
			existingItem.setCost(item.getCost());
			existingItem.setQuantity(item.getQuantity());
			return itemRepository.save(existingItem);
		}
		return null;
	}

	@Override
	public boolean deleteItem(int id) {
		Item item= getItemById(id);
		if(item!=null) {
			itemRepository.delete(item);
			return true;
		}else {
			return false;
		}
	}

}
