package com.ty.foodjoin.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoin.dao.ProductDao;
import com.ty.foodjoin.dto.Product;
import com.ty.foodjoin.repository.ProductRepository;

@Repository
public class ProductDaoImpl implements ProductDao {
	@Autowired
	ProductRepository repository;

	@Override
	public Product saveProduct(Product product) {
		// Product resp =(Product)repository.save(product);
		return repository.save(product);
	}

	@Override
	public Product getProductById(int id) {
		Optional<Product> optional = repository.findById(id);
		if(optional.isPresent()) {
		return optional.get();
	}
		return null;
	}
	
	@Override
	public List<Product> getAllProducts() {
		return repository.findAll();
	}

	@Override
	public Product updateProduct(int id, Product product) {
		Product existingProduct = getProductById(id);
		if (existingProduct != null) {
			existingProduct.setName(product.getName());
			existingProduct.setCost(product.getCost());
			existingProduct.setDescription(product.getDescription());
			existingProduct.setFood_type(product.getFood_type());
			existingProduct.setCategory(product.getCategory());

			return repository.save(existingProduct);
		}

		return null;
	}

	@Override
	public boolean deleteProduct(int id) {
		Product product = getProductById(id);
		if (product != null) {
			repository.delete(product);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Product getProductByName(String name) {
		Optional<Product> product = repository.findByName(name);
		if (product.isPresent()) {
			return product.get();
		}
		return null;
	}
}
