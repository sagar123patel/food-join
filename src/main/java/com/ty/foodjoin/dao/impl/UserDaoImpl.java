package com.ty.foodjoin.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ty.foodjoin.dao.UserDao;
import com.ty.foodjoin.dto.User;
import com.ty.foodjoin.repository.UserRepository;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private UserRepository repository;

	public User saveUser(User user) {
		User userResp = (User) repository.save(user);
		return userResp;
	}

	public User getUserById(int id) {
		Optional<User> optional = repository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	public List<User> getAllUsers() {
		return repository.findAll();
	}

	public User updateUser(int id, User user) {
		User existingUser = getUserById(id);
		if (existingUser != null) {
			existingUser.setName(user.getName());
			existingUser.setEmail(user.getEmail());
			existingUser.setPassword(user.getPassword());
			existingUser.setPhone_No(user.getPhone_No());
			return repository.save(existingUser);
		}
		return null;
	}

	public boolean deleteUser(int id) {
		User user = getUserById(id);
		if (user != null) {
			repository.delete(user);
			return true;
		} else {
			return false;
		}
	}

	public User validateUser(String email, String password) {
		User user =repository.validateUser(email, password);
		return user;
	}

	public User getUserByName(String name) {

		Optional<User> user = repository.findByName(name);
		if (user.isPresent()) {
			return user.get();
		}
		return null;
	}
}
