package com.ty.foodjoin.dao;

import java.util.List;

import com.ty.foodjoin.dto.McOrder;

public interface McOrderDao {

	
	public McOrder saveMcOrder(McOrder  mcOrder);
	
	public McOrder getMcOrderById(int id);
	
	public List<McOrder> getAllMcOrders();
	
	public McOrder updateMcOrders(int id,McOrder mcOrder);
	
	public boolean deleteMcOrder(int id);
	
	
}
