package com.ty.foodjoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodJoinApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodJoinApplication.class, args);
	}

}
