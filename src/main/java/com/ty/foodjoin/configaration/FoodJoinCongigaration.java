package com.ty.foodjoin.configaration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class FoodJoinCongigaration {
	@Bean 
	public Docket getDoket() {
		springfox.documentation.service.Contact contact =new springfox.documentation.service.Contact("Alpha Team","https://testtantra.com","alpha@ty.com");
		@SuppressWarnings("rawtypes")
		List<VendorExtension> extensions =new ArrayList<VendorExtension>();
		ApiInfo apiInfo =new ApiInfo("Foos Join API document",
				"API's to place the order to customer in-house",
				"TYP-FoodJoin-Snapshoot 1.0.1",
				"https://testyantra.com/", contact,
				"Licence 11001","https://jsonbeautifier.org/",
				extensions);
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.ty.foodjoin"))
				.build()
				.apiInfo(apiInfo)
				.useDefaultResponseMessages(false);
				
	}
}
