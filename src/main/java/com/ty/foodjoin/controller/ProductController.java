package com.ty.foodjoin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoin.dto.Product;
import com.ty.foodjoin.service.ProductService;
import com.ty.foodjoin.util.ResponseStructure;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ProductController {

	@Autowired
	private ProductService service;

	@PostMapping("/product")
	public ResponseEntity<ResponseStructure<Product>> saveProduct(@RequestBody Product product) {
		return service.saveProduct(product);
	}

	@GetMapping("/product")
	public ResponseEntity<ResponseStructure<List<Product>>> getAll() {
		return service.getAllProducts();
	}

	@GetMapping("/product/{id}")
	@ApiOperation("To Get Product Data by Id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retrived Product"),
			@ApiResponse(code = 400, message = "ID not Found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<ResponseStructure<Product>> getProductById(@PathVariable int id) {
		return service.getProductById(id);
	}

	@PutMapping("/product/{id}")
	@ApiOperation("To Update Product Data by Id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Update Product"),
			@ApiResponse(code = 400, message = "ID not Found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	public ResponseEntity<ResponseStructure<Product>> updateProduct(@PathVariable int id,
			@RequestBody Product product) {
		return service.updateProduct(id, product);
	}

	@DeleteMapping("/product/{id}")
	public boolean deleteProduct(@PathVariable int id) {
		return service.deleteProduct(id);
	}

	@GetMapping("/product/{name}")
	@ApiOperation("To get product data by name")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retrived product"),
			@ApiResponse(code = 404, message = "Name not found"),
			@ApiResponse(code = 500, message = "Internal server found") })
	public ResponseEntity<ResponseStructure<Product>> getUserBbyName(
			@ApiParam("Name to get the product") @PathVariable String name) {
		return service.getProductByName(name);
	}
}