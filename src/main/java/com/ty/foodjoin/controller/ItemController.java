package com.ty.foodjoin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoin.dto.Item;
import com.ty.foodjoin.service.ItemService;
import com.ty.foodjoin.util.ResponseStructure;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "to unset to save get update delete ")
public class ItemController {
	
	@Autowired
	private ItemService itemService;

	@PostMapping("/item")
	public ResponseEntity<ResponseStructure<Item>> saveItem(@RequestBody Item item) {
		return itemService.saveItem(item);
	}

	@GetMapping("/item/{id}")
	@ApiOperation("To get Item Data by id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retrived Item"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal server found") })
	public ResponseEntity<ResponseStructure<Item>> getItemById(@ApiParam("Id to get the Item") @PathVariable int id) {
		return itemService.getItemById(id);
	}

	@GetMapping("/item")
	public ResponseEntity<ResponseStructure<List<Item>>> getAllItems() {
		return itemService.getAllItems();
	}

	@PutMapping("/item/{id}")
	public ResponseEntity<ResponseStructure<Item>> updateItem(@PathVariable int id, Item item) {
		return itemService.updateItem(id, item);

	}

	@DeleteMapping("/item/{id}")
	public ResponseEntity<ResponseStructure<String>> deleteItem(@RequestParam int id) {
		return itemService.deleteItem(id);
	}
	

}
