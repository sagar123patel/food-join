package com.ty.foodjoin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoin.dto.McOrder;
import com.ty.foodjoin.service.McOrderService;
import com.ty.foodjoin.util.ResponseStructure;


@RestController
public class McOrderController {

	@Autowired
	private McOrderService mcOrderService;

	@PostMapping("/order")
	public ResponseEntity<ResponseStructure<McOrder>> saveProduct(@RequestBody McOrder mcOrder) {
		return mcOrderService.saveMcOrder(mcOrder);
	}

	@GetMapping("/order")
	public ResponseEntity<ResponseStructure<List<McOrder>>> getAll() {
		return mcOrderService.getAllMcOrders();
	}

	@GetMapping("/order/{id}")
	public ResponseEntity<ResponseStructure<McOrder>> getProductById(@PathVariable int id) {
		return mcOrderService.getMcOrderById(id);
	}

	@PutMapping("/order/{id}")
	public ResponseEntity<ResponseStructure<McOrder>> updateProduct(@PathVariable int id,
			@RequestBody McOrder mcOrder) {
		return mcOrderService.updateMcOrder(id, mcOrder);
	}

	@DeleteMapping("/order")
	public boolean deleteProduct(@RequestParam int id) {
		return mcOrderService.deleteMcOrder(id);
	}

}
