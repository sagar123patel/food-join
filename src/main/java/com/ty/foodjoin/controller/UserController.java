package com.ty.foodjoin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoin.dto.Login;
import com.ty.foodjoin.dto.User;
import com.ty.foodjoin.service.UserService;
import com.ty.foodjoin.util.ResponseStructure;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "to uset to save get update delete ")
public class UserController {
	
	@Autowired
	private UserService service;

	@PostMapping("user/login")
	@ApiOperation("To User Login")
	@ApiResponses({@ApiResponse(code = 200,message = "Login success"),
		@ApiResponse(code = 404,message = " Did not Find"),
		@ApiResponse(code = 500,message = "Internal Server error")})
	public ResponseEntity<ResponseStructure<User>> loginStaff(@ApiParam("Login for User") @RequestBody Login dto) {
		String email = dto.getEmail();
		String password = dto.getPassword();
		System.out.println(dto.getEmail() + dto.getPassword());
		return service.validateUser(email, password);
	}
	
	@PostMapping("/user")
	@ApiOperation("To Save User Data ")
	@ApiResponses({@ApiResponse(code = 200,message = "Save success"),
		@ApiResponse(code = 404,message = " Did not Find"),
		@ApiResponse(code = 500,message = "Internal Server error")})
	public ResponseEntity<ResponseStructure<User>> saveUser(@ApiParam("Save to User") @RequestBody User user) {
		return service.saveUser(user);
	}
	
	@GetMapping("/user")
	public ResponseEntity<ResponseStructure<List<User>>> getAllUser() {
		return service.getAllUser();
	}
	
	@GetMapping("/user/{id}")
	@ApiOperation("To Get User Data by Id")
	@ApiResponses({@ApiResponse(code = 200, message ="Retrived User"),
		@ApiResponse(code = 400,message="ID not Found"),
		@ApiResponse(code = 500,message="Internal server error")})
	public ResponseEntity<ResponseStructure<User>> getUserById(@PathVariable int id) {
		return service.getById(id);
	}
	
	@PutMapping("/user/{id}")
	public ResponseEntity<ResponseStructure<User>> updateUser(@PathVariable int id,@RequestBody User user) {
		return service.updateUser(id,user);
	}
	
	@DeleteMapping("/user")
	public boolean deleteUser(@RequestParam int id) {
		return service.deleteUser(id);
	}
	
	@GetMapping("/user/{name}")
	@ApiOperation("To get user data by name")
	@ApiResponses({@ApiResponse(code =200, message="Retrived user"),
			@ApiResponse(code = 404, message= "Name not found"),
			@ApiResponse(code = 500, message = "Internal server found")})	
	public ResponseEntity<ResponseStructure<User>> getUserByName(@ApiParam("Name to get the user")@PathVariable String name) {
		return service.getUserByName(name);
	}
	
}
