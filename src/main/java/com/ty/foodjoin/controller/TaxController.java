package com.ty.foodjoin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ty.foodjoin.dto.Tax;
import com.ty.foodjoin.service.impl.TaxServiceImpl;
import com.ty.foodjoin.util.ResponseStructure;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "To Save Get Update And Delete")
public class TaxController {

	@Autowired
	private TaxServiceImpl taxServiceImpl;

	@PostMapping("/tax")
	public ResponseEntity<ResponseStructure<Tax>> saveTax(@RequestBody Tax tax) {
		return taxServiceImpl.saveTax(tax);
	}

	@GetMapping("/tax/{id}")
	@ApiOperation("To get tax data by id")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retrived tax"),
			@ApiResponse(code = 404, message = "Id not found"),
			@ApiResponse(code = 500, message = "Internal server found") })
	public ResponseEntity<ResponseStructure<Tax>> getTaxbyId(@ApiParam("Id to get the tax") @PathVariable int id) {
		return taxServiceImpl.getTaxById(id);
	}

	@GetMapping("/tax")
	public ResponseEntity<ResponseStructure<List<Tax>>> getAllTax() {
		return taxServiceImpl.getAllTaxs();
	}

	@PutMapping("/tax/{id}")
	public ResponseEntity<ResponseStructure<Tax>> updateTax(@PathVariable int id, Tax tax) {
		return taxServiceImpl.updateTax(id, tax);

	}

	@DeleteMapping("/tax/{id}")
	public ResponseEntity<ResponseStructure<String>> deleteTax(@RequestParam int id) {
		return taxServiceImpl.deleteTax(id);
	}

}
