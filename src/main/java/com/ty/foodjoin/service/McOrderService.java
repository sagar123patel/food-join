package com.ty.foodjoin.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoin.dto.McOrder;
import com.ty.foodjoin.util.ResponseStructure;

public interface McOrderService {

	
	public  ResponseEntity<ResponseStructure<McOrder>> saveMcOrder(McOrder taxO);

	public ResponseEntity<ResponseStructure<McOrder>> getMcOrderById(int id);

	public ResponseEntity<ResponseStructure<List<McOrder>>> getAllMcOrders();

	public ResponseEntity<ResponseStructure<McOrder>> updateMcOrder(int id, McOrder tax);

	public boolean deleteMcOrder(int id);
}
