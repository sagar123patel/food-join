package com.ty.foodjoin.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoin.dto.Product;
import com.ty.foodjoin.util.ResponseStructure;

public interface ProductService {

	public  ResponseEntity<ResponseStructure<Product>> saveProduct(Product product);

	public ResponseEntity<ResponseStructure<Product>> getProductById(int id);

	public ResponseEntity<ResponseStructure<List<Product>>> getAllProducts();

	public ResponseEntity<ResponseStructure<Product>> updateProduct(int id, Product product);

	public boolean deleteProduct(int id);

	public ResponseEntity<ResponseStructure<Product>> getProductByName(String name);
	
}
