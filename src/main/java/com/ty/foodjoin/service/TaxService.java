package com.ty.foodjoin.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoin.dto.Tax;
import com.ty.foodjoin.util.ResponseStructure;

public interface TaxService {

	public  ResponseEntity<ResponseStructure<Tax>> saveTax(Tax taxO);

	public ResponseEntity<ResponseStructure<Tax>> getTaxById(int id);

	public ResponseEntity<ResponseStructure<List<Tax>>> getAllTaxs();

	public ResponseEntity<ResponseStructure<Tax>> updateTax(int id, Tax tax);

	public ResponseEntity<ResponseStructure<String>> deleteTax(int id);
	
	
}
