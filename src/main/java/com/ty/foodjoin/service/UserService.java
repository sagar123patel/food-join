package com.ty.foodjoin.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoin.dto.User;
import com.ty.foodjoin.util.ResponseStructure;

public interface UserService {

		public ResponseEntity<ResponseStructure<User>> saveUser(User user);

		public ResponseEntity<ResponseStructure<User>> getById(int id);

		public ResponseEntity<ResponseStructure<List<User>>> getAllUser();

		public ResponseEntity<ResponseStructure<User>> updateUser(int id,User user);

		public boolean deleteUser(int id);

		public ResponseEntity<ResponseStructure<User>>  validateUser(String email, String password);

		public ResponseEntity<ResponseStructure<User>> getUserByName(String name);
}
