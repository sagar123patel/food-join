package com.ty.foodjoin.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ty.foodjoin.dto.Item;
import com.ty.foodjoin.util.ResponseStructure;

public interface ItemService {
	
	public  ResponseEntity<ResponseStructure<Item>> saveItem(Item item);

	public ResponseEntity<ResponseStructure<Item>> getItemById(int id);

	public ResponseEntity<ResponseStructure<List<Item>>> getAllItems();

	public ResponseEntity<ResponseStructure<Item>> updateItem(int id, Item item);

	public ResponseEntity<ResponseStructure<String>> deleteItem(int id);

}
