package com.ty.foodjoin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoin.dao.ProductDao;
import com.ty.foodjoin.dto.Product;
import com.ty.foodjoin.exception.IDNotFoundException;
import com.ty.foodjoin.service.ProductService;
import com.ty.foodjoin.util.ResponseStructure;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao dao;

	@Override
	public ResponseEntity<ResponseStructure<Product>> saveProduct(Product product) {
		ResponseStructure<Product> structure = new ResponseStructure<Product>();
		ResponseEntity<ResponseStructure<Product>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(dao.saveProduct(product));
		entity = new ResponseEntity<ResponseStructure<Product>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Product>> getProductById(int id) {
		ResponseStructure<Product> structure = new ResponseStructure<Product>();
		ResponseEntity<ResponseStructure<Product>> entity = null;
		Product product = dao.getProductById(id);
		if (product != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(dao.getProductById(id));
			entity = new ResponseEntity<ResponseStructure<Product>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Product ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Product>>> getAllProducts() {
		ResponseStructure<List<Product>> structure = new ResponseStructure<List<Product>>();
		ResponseEntity<ResponseStructure<List<Product>>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(dao.getAllProducts());
		entity = new ResponseEntity<ResponseStructure<List<Product>>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Product>> updateProduct(int id, Product product) {
		ResponseStructure<Product> structure = new ResponseStructure<Product>();
		ResponseEntity<ResponseStructure<Product>> entity = null;
		Product product2 = dao.updateProduct(id, product);
		if (product2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(product);
			entity = new ResponseEntity<ResponseStructure<Product>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Product ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public boolean deleteProduct(int id) {
		return dao.deleteProduct(id);
	}

	@Override
	public ResponseEntity<ResponseStructure<Product>> getProductByName(String name) {
		ResponseStructure<Product> responsestructure = new ResponseStructure<Product>();
		ResponseEntity<ResponseStructure<Product>> responseEntity;
		Product product = dao.getProductByName(name);
		if (product != null) {
			responsestructure.setStatus(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData(product);
			responseEntity = new ResponseEntity<ResponseStructure<Product>>(responsestructure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Product  : " + name + " is not found");
		}
		return responseEntity;
	}
}
