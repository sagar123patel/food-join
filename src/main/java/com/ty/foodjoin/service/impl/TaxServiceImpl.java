package com.ty.foodjoin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoin.dao.TaxDao;
import com.ty.foodjoin.dto.Tax;
import com.ty.foodjoin.exception.IDNotFoundException;
import com.ty.foodjoin.service.TaxService;
import com.ty.foodjoin.util.ResponseStructure;

@Service
public class TaxServiceImpl implements TaxService {

	@Autowired
	private TaxDao taxDao;

	@Override
	public ResponseEntity<ResponseStructure<Tax>> saveTax(Tax tax) {
		ResponseStructure<Tax> responsestructure = new ResponseStructure<Tax>();
		responsestructure.setStatus(HttpStatus.OK.value());
		responsestructure.setMessage("Success");
		responsestructure.setData(taxDao.saveTax(tax));
		ResponseEntity<ResponseStructure<Tax>> responseEntity = new ResponseEntity<ResponseStructure<Tax>>(
				responsestructure, HttpStatus.OK);
		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Tax>> getTaxById(int id) {
		ResponseStructure<Tax> responsestructure = new ResponseStructure<Tax>();
		ResponseEntity<ResponseStructure<Tax>> responseEntity;
		Tax tax = taxDao.getTaxById(id);
		if (tax != null) {
			responsestructure.setStatus(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData(tax);
			responseEntity = new ResponseEntity<ResponseStructure<Tax>>(responsestructure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Tax  : " + id + " is not found");
		}

		return responseEntity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Tax>>> getAllTaxs() {
		ResponseStructure<List<Tax>> responseStructure = new ResponseStructure<List<Tax>>();
		responseStructure.setStatus(HttpStatus.OK.value());
		responseStructure.setMessage("Success");
		responseStructure.setData(taxDao.getAllTaxs());
		return new ResponseEntity<ResponseStructure<List<Tax>>>(responseStructure, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseStructure<Tax>> updateTax(int id, Tax tax) {
		ResponseStructure<Tax> responseStructure = new ResponseStructure<Tax>();
		ResponseEntity<ResponseStructure<Tax>> entity = null;
		Tax returnedTax = taxDao.updateTax(id, tax);
		if (returnedTax != null) {
			responseStructure.setStatus(HttpStatus.OK.value());
			responseStructure.setMessage("Success");
			responseStructure.setData(returnedTax);
			entity = new ResponseEntity<ResponseStructure<Tax>>(responseStructure, HttpStatus.NOT_FOUND);
		} else {
			throw new IDNotFoundException("Tax id: " + id + " is not found");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteTax(int id) {
		ResponseStructure<String> responsestructure = new ResponseStructure<String>();
		ResponseEntity<ResponseStructure<String>> responseEntity = null;
		if (taxDao.deleteTax(id)) {
			responsestructure.setStatus(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData("Tax deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.OK);
		} else {
			responsestructure.setStatus(HttpStatus.NOT_FOUND.value());
			responsestructure.setMessage("Id : " + id + "is not found");
			responsestructure.setData("No Tax is deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.NOT_FOUND);
		}

		return responseEntity;

	}
}
