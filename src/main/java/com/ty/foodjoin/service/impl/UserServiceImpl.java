package com.ty.foodjoin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoin.dao.UserDao;
import com.ty.foodjoin.dto.User;
import com.ty.foodjoin.exception.IDNotFoundException;
import com.ty.foodjoin.service.UserService;
import com.ty.foodjoin.util.ResponseStructure;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public ResponseEntity<ResponseStructure<User>> saveUser(User user) {
		ResponseStructure<User> structure = new ResponseStructure<User>();
		ResponseEntity<ResponseStructure<User>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(userDao.saveUser(user));
		entity = new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> getById(int id) {
		ResponseStructure<User> structure = new ResponseStructure<User>();
		ResponseEntity<ResponseStructure<User>> entity = null;
		User user = userDao.getUserById(id);
		if (user != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(userDao.getUserById(id));
			entity = new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);

		} else {
			throw new IDNotFoundException("User ID :" + id + "does not exist");
		}
		return entity;

	}

	@Override
	public ResponseEntity<ResponseStructure<List<User>>> getAllUser() {
		ResponseStructure<List<User>> structure = new ResponseStructure<List<User>>();
		ResponseEntity<ResponseStructure<List<User>>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(userDao.getAllUsers());
		entity = new ResponseEntity<ResponseStructure<List<User>>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> updateUser(int id, User user) {
		ResponseStructure<User> structure = new ResponseStructure<User>();
		ResponseEntity<ResponseStructure<User>> entity = null;

		User user2 = userDao.updateUser(id, user);
		if (user2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(user);
			entity = new ResponseEntity<ResponseStructure<User>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("User ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public boolean deleteUser(int id) {
		return userDao.deleteUser(id);
	}

	@Override
	public ResponseEntity<ResponseStructure<User>> validateUser(String email, String password) {

		ResponseStructure<User> structuer = new ResponseStructure<User>();
		ResponseEntity<ResponseStructure<User>> entity;
		User user = userDao.validateUser(email, password);
		if (user != null) {
			structuer.setStatus(HttpStatus.OK.value());
			structuer.setMessage("successfull");
			structuer.setData(userDao.validateUser(email, password));
			entity = new ResponseEntity<ResponseStructure<User>>(structuer, HttpStatus.OK);
		} else {
			structuer.setStatus(HttpStatus.NOT_FOUND.value());
			structuer.setMessage("email :" + email + " password:" + password + " !!!NOT-MATCHING!!!!!!");
			structuer.setData(userDao.validateUser(email, password));
			entity = new ResponseEntity<ResponseStructure<User>>(structuer, HttpStatus.NOT_FOUND);
		}
		return entity;

	}

	@Override
	public ResponseEntity<ResponseStructure<User>> getUserByName(String name) {
		ResponseStructure<User> responsestructure = new ResponseStructure<User>();
		ResponseEntity<ResponseStructure<User>> responseEntity;

		User user = userDao.getUserByName(name);
		if (user != null) {
			responsestructure.setStatus(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData(user);
			responseEntity = new ResponseEntity<ResponseStructure<User>>(responsestructure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("User  : " + name + " is not found");

		}

		return responseEntity;
	}
}