package com.ty.foodjoin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoin.dao.McOrderDao;
import com.ty.foodjoin.dto.McOrder;
import com.ty.foodjoin.exception.IDNotFoundException;
import com.ty.foodjoin.service.McOrderService;
import com.ty.foodjoin.util.ResponseStructure;

@Service
public class McOrderServiceImpl implements McOrderService {
	@Autowired
	private McOrderDao mcOrderDao;

	@Override
	public ResponseEntity<ResponseStructure<McOrder>> saveMcOrder(McOrder mcOrder) {
		ResponseStructure<McOrder> structure = new ResponseStructure<McOrder>();
		ResponseEntity<ResponseStructure<McOrder>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(mcOrderDao.saveMcOrder(mcOrder));
		entity = new ResponseEntity<ResponseStructure<McOrder>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<McOrder>> getMcOrderById(int id) {
		ResponseStructure<McOrder> structure = new ResponseStructure<McOrder>();
		ResponseEntity<ResponseStructure<McOrder>> entity = null;
		McOrder product = mcOrderDao.getMcOrderById(id);
		if (product != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(mcOrderDao.getMcOrderById(id));
			entity = new ResponseEntity<ResponseStructure<McOrder>>(structure, HttpStatus.OK);

		} else {
			throw new IDNotFoundException("McOrder ID :" + id + "does not exist");
		}
		return entity;

	}

	@Override
	public ResponseEntity<ResponseStructure<List<McOrder>>> getAllMcOrders() {
		ResponseStructure<List<McOrder>> structure = new ResponseStructure<List<McOrder>>();
		ResponseEntity<ResponseStructure<List<McOrder>>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(mcOrderDao.getAllMcOrders());
		entity = new ResponseEntity<ResponseStructure<List<McOrder>>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<McOrder>> updateMcOrder(int id, McOrder mcOrder) {
		ResponseStructure<McOrder> structure = new ResponseStructure<McOrder>();
		ResponseEntity<ResponseStructure<McOrder>> entity = null;

		McOrder mcOrder2 = mcOrderDao.updateMcOrders(id, mcOrder);
		if (mcOrder2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(mcOrder);
			entity = new ResponseEntity<ResponseStructure<McOrder>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Product ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public boolean deleteMcOrder(int id) {
		return mcOrderDao.deleteMcOrder(id);
	}

}
