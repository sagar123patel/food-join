package com.ty.foodjoin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ty.foodjoin.dao.impl.ItemDaoImp;
import com.ty.foodjoin.dto.Item;
import com.ty.foodjoin.exception.IDNotFoundException;
import com.ty.foodjoin.service.ItemService;
import com.ty.foodjoin.util.ResponseStructure;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDaoImp itemDaoImp;

	@Override
	public ResponseEntity<ResponseStructure<Item>> saveItem(Item item) {
		ResponseStructure<Item> structure = new ResponseStructure<Item>();
		ResponseEntity<ResponseStructure<Item>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(itemDaoImp.saveItem(item));
		entity = new ResponseEntity<ResponseStructure<Item>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Item>> getItemById(int id) {
		ResponseStructure<Item> structure = new ResponseStructure<Item>();
		ResponseEntity<ResponseStructure<Item>> entity = null;
		Item product = itemDaoImp.getItemById(id);
		if (product != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(itemDaoImp.getItemById(id));
			entity = new ResponseEntity<ResponseStructure<Item>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Product ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<List<Item>>> getAllItems() {
		ResponseStructure<List<Item>> structure = new ResponseStructure<List<Item>>();
		ResponseEntity<ResponseStructure<List<Item>>> entity = null;
		structure.setStatus(HttpStatus.OK.value());
		structure.setMessage("success");
		structure.setData(itemDaoImp.getAllItem());
		entity = new ResponseEntity<ResponseStructure<List<Item>>>(structure, HttpStatus.OK);
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<Item>> updateItem(int id, Item item) {
		ResponseStructure<Item> structure = new ResponseStructure<Item>();
		ResponseEntity<ResponseStructure<Item>> entity = null;

		Item item2 = itemDaoImp.updateItem(id, item);
		if (item2 != null) {
			structure.setStatus(HttpStatus.OK.value());
			structure.setMessage("success");
			structure.setData(item);
			entity = new ResponseEntity<ResponseStructure<Item>>(structure, HttpStatus.OK);
		} else {
			throw new IDNotFoundException("Item ID :" + id + "does not exist");
		}
		return entity;
	}

	@Override
	public ResponseEntity<ResponseStructure<String>> deleteItem(int id) {
		ResponseStructure<String> responsestructure = new ResponseStructure<String>();
		ResponseEntity<ResponseStructure<String>> responseEntity = null;
		if (itemDaoImp.deleteItem(id)) {
			responsestructure.setStatus(HttpStatus.OK.value());
			responsestructure.setMessage("Success");
			responsestructure.setData("Item deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.OK);
		} else {
			responsestructure.setStatus(HttpStatus.NOT_FOUND.value());
			responsestructure.setMessage("Id : " + id + "is not found");
			responsestructure.setData("No Item is deleted");
			responseEntity = new ResponseEntity<ResponseStructure<String>>(responsestructure, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}
}
